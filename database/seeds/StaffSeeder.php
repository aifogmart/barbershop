<?php

use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orgstaff')->delete();

        DB::table('orgstaff')->insert([
            'id' => 1,
            'orgid' => 1,
            'lname' => 'Иванов',
            'fname' => 'Иван',
            'mname' => 'Иванович',
            'whocrt' => 'init',
            'whoupd' => 'init',
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
        DB::table('orgstaff')->insert([
            'id' => 2,
            'orgid' => 2,
            'lname' => 'Сидоров',
            'fname' => 'Петр',
            'mname' => 'Аркадьевич',
            'whocrt' => 'init',
            'whoupd' => 'init',
            'created_at'=>now(),
            'updated_at'=>now()
        ]);

        DB::table('orgstaff')->insert([
            'id' => 3,
            'orgid' => 3,
            'lname' => 'Семенов',
            'fname' => 'Семен',
            'mname' => 'Семенович',
            'whocrt' => 'init',
            'whoupd' => 'init',
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
        DB::table('orgstaff')->insert([
            'id' => 4,
            'orgid' => 3,
            'lname' => 'Петров',
            'fname' => 'Петр',
            'mname' => 'Петрович',
            'whocrt' => 'init',
            'whoupd' => 'init',
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
        DB::table('orgstaff')->insert([
            'id' => 5,
            'orgid' => 3,
            'lname' => 'Алексеев',
            'fname' => 'Алексей',
            'mname' => 'Петрович',
            'whocrt' => 'init',
            'whoupd' => 'init',
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
    }
}
