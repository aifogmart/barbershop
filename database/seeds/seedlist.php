<?php

use Illuminate\Database\Seeder;
use App\universal;


class seedlist extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $load=new universal('seedlist.csv');
        $load->run();
    }
}
