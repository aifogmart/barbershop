<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->cleanDatabase();
        $this->call(seedlist::class);
    }
    //Зачистим базу без учета FK
    //исключив таблицу миграции
    private function cleanDatabase()
    {
       $tableNames = Schema::getConnection()
                                ->getDoctrineSchemaManager()
                                ->listTableNames();
        Schema::disableForeignKeyConstraints();
        foreach ($tableNames as $table) {
            if('migrations'==$table) continue;
            DB::table($table)->truncate();
        }
        Schema::enableForeignKeyConstraints();
    }

}
