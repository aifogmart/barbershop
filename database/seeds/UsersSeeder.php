<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'board@itqua.ru',
            'password' => bcrypt('5rn633da')
        ]);
        DB::table('users')->insert([
            'name' => 'test user',
            'email' => 'test@itqua.ru',
            'password' => bcrypt('test123'),
            'staffid' => 1
        ]);
        
        DB::table('users')->insert([
            'lname' => 'Шевченко',
            'fname' => 'Сергей',
            'name' => 'Шевченко Сергей',
            'email' => 'sns@itqua.ru',
            'password' => '$2y$10$SbPzFfwKH7XQEUchrSicY.PaRKBFe9dsd8I7dULeIN3BHsCh5wOuC',
            'staffid' => 12
        ]);

        DB::table('users')->insert([
            'name' => 'Евдокимов Александр',
            'email' => 'aevdokimov@itqua.ru',
            'password' => bcrypt('test123'),
            'staffid' => 3
        ]);
        DB::table('users')->insert([
            'name' => 'Щетинкин Станислав',
            'email' => 'sshchetinkin@itqua.ru',
            'password' => bcrypt('test123'),
            'staffid' => 3
        ]);
        DB::table('users')->insert([
            'name' => 'Валентин Дмитрий',
            'email' => 'dvalentin@itqua.ru',
            'password' => bcrypt('test123'),
            'staffid' => 3
        ]);
        DB::table('users')->insert([
            'name' => 'Шевчук Сергей',
            'email' => 'osetsky@itqua.ru',
            'password' => bcrypt('test123'),
            'staffid' => 2
        ]);

        DB::table('users')->insert([
            'name' => 'менеджер 1',
            'email' => 'mngr1@test.com',
            'password' => bcrypt('test123'),
            'staffid' => 14
        ]);
        DB::table('users')->insert([
            'name' => 'менеджер 2',
            'email' => 'mngr2@test.com',
            'password' => bcrypt('test123'),
            'staffid' => 15
        ]);
        DB::table('users')->insert([
            'lname' => 'Шевченко',
            'fname' => 'Евгений',
            'mname' => 'Сергеевич',
            'name' => 'менеджер по рекламе',
            'email' => 'afroim@yandex.ru',
            'password' => bcrypt('test123'),
            'staffid' => 17
        ]);
    }
}
