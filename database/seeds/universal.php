<?php
namespace App;
use DB;
use Exception;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Seeder;
use Illuminate\Database\Query\Builder;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

//Универсальный сидер читающий XSLX\seedlist.csv
//И по порядку из него загружает данные из XSLX\tables

//Таблица для загрузки должна быть указана в  ./database/seeds/XLSX/seedlist.csv
//Данные должны лежать в каталоге ./database/seeds/XLSX/tables
//имя файла должно совпадать с именем таблицы, расширение файла  xlsx или csv
//формат данных xlsx или csv
//В первой строке файла должны содержаться имена полей совпадающие с
//именами полей таблицы
//Порядок определенный в seedlist.csv является порядком загрузки данных

//csv-файл удобно получить экспортом из MySql. Сразу будет правильная кодировка и кавычки в нужных местах.
//Но нужно заменить NULL на пустое место: ",NULL," -> ",,"

class universal
{
    protected $file4list;
    protected $dir4list;
    public function __construct($file){
        $this->file4list=$file;
        $this->dir4list="./database/seeds/XLSX";
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Загрузка данных в таблицы:\n";
        $dir_xlsx="./database/seeds/XLSX/tables";
        $ctl_file_path=$this->dir4list.DIRECTORY_SEPARATOR.$this->file4list;

        $ctl_file= ReaderFactory::create(Type::CSV);
        $list_object=array();
        try{
            $ctl_file->open($ctl_file_path);
            foreach ($ctl_file->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    if(mb_strlen($row[0])==0) continue;

                    array_push($list_object,
                                      mb_ereg_replace("(^\s+)|(\s+$)/us","",$row[0]
                    ));
                }
            }
            $ctl_file->close();
            $nobj=$this->hasTable($list_object);
            if(count($nobj)>0){
                throw new Exception("В базе не существуют таблицы: ".join(",",$nobj) );
            }
            $nobj=$this->fileExists($dir_xlsx,$list_object);
            if(count($nobj)>0){
                throw new Exception("Не существуют файлов для загрузки: ".join(",",$nobj));
            }

            $this->truncate_data($list_object);
            $ret=-1;
            foreach ($list_object as  $obj) {
                $ret=$this->load_file($dir_xlsx,$obj);
                if($ret==0) break;
            }
            if($ret==0){
                print "Произошла ошибка база в неконсистентном состоянии";
            }
        }
        catch(Exception $e){
            echo "Ошибка работы с контрольным файлом ".$ctl_file_path.":".$e->getMessage()."\n";

        }
    }

    private function hasTable($list_object){
        $tablename=array();
        foreach(array_reverse($list_object) as $obj){
           if(!Schema::hasTable($obj)) {
            array_push($tablename,$obj);
           }
        }
        return $tablename;
    }
    private function  fileExists($dir, $list_object) {
        $nfile=array();
        foreach($list_object as $object) {
            $filepathx = $dir . DIRECTORY_SEPARATOR . $object.".xlsx";
            $filepathc = $dir . DIRECTORY_SEPARATOR . $object.".csv";
            if(file_exists($filepathx)){

            }elseif ($filepathc) {

            }else{
                array_push($nfile,$object);
            }
        }
        return $nfile;
    }

    private function truncate_data($list_object){
        //снесем данные в обратном порядке
        foreach(array_reverse($list_object) as $obj){
           if(Schema::hasTable($obj)) {
                DB::table($obj)->delete(); //truncate не используется т.к. есть FK
           }
        }
    }

   private function load_file($dir,$object){
       $ret=0;
       print "Обрабатывается объект ".$object."\n";
        $filepathx = $dir . DIRECTORY_SEPARATOR . $object.".xlsx";
        $filepathc = $dir . DIRECTORY_SEPARATOR . $object.".csv";
        $filepath = $dir . DIRECTORY_SEPARATOR . $object;
        $tp="";
        if(file_exists($filepathx)){
            $filepath=$filepathx;
            $tp=Type::XLSX;
        }elseif ($filepathc) {
            $filepath=$filepathc;
            $tp=Type::CSV;
        }else{
            print "Файлов с расширение xlsx или csv с именем ".$filepath." не существует";
            return 0;
        }

        if(!Schema::hasTable($object)){
            print "Таблица".$object." не существует";
            return 0;
        }
        try{
            $reader= ReaderFactory::create($tp);
            $reader->open($filepath);
            $cnt=-1;
            $fields = array();
            $table =  array();
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    //Формируем массив для загрузки
                    $cnt++;
                    if($cnt==0){
                        $fields=$this->getFields($row);
                        continue;
                    }
                    $r = $fields;
                    foreach($r as $key => $val){
                        $v=mb_ereg_replace("[ \t\n\r]+", " ",mb_ereg_replace("(^\s+)|(\s+$)/us","",$row[$val]));
                        if(mb_strlen($v)==0) $v=null;
                        $r[$key]=$v;
                    }
                    array_push($table,$r);
                }
            }
            $reader->close();

            //Добавим служебные поля
            $table=$this->add_fields($object,$table);

            //Загружаем данные
            foreach($table as $id => $val){
                DB::table($object)->insert($val);
            }
            print "\tВставлено: ".count($table)." записей\n";
            print "Окончена обработка объекта ".$object."\n";
            //print_r($table);
            $ret=1;

        }
        catch(Exception $e){
            echo "\tОшибка обработки объекта  ". $object.":".$e->getMessage()."\n";

        }
        return $ret;
   }

   private function add_fields($object, $table) {
            //Загоняем данные created_at, updated_at, whocrt whoupd
            $who="init";
            $whn=now()->toDateTimeString();
            if(Schema::hasColumn($object, 'created_at')){
                foreach($table as $id => $val){
                    $val['created_at']=$whn;
                    $table[$id]=$val;
                }
            }
            if(Schema::hasColumn($object, 'updated_at')){
                foreach($table as $id => $val){
                    $val['updated_at']=$whn;
                    $table[$id]=$val;
                }
            }
            if(Schema::hasColumn($object, 'whocrt')){
                foreach($table as $id => $val){
                    $val['whocrt']=$who;
                    $table[$id]=$val;
                }
            }
            if(Schema::hasColumn($object, 'whoupd')){
                foreach($table as $id => $val){
                    $val['whoupd']=$who;
                    $table[$id]=$val;
                }
            }
        return    $table;
   }

   private function getFields($row){
        $fa=array();
        foreach($row as $k => $v){
            $fa[$v]=mb_ereg_replace("[ \t\n\r]+", " ",mb_ereg_replace("(^\s+)|(\s+$)/us","",$k));
        }
        return $fa;
   }




}
