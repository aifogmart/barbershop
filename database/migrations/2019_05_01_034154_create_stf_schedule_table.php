<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStfScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stf_schedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('stfid')->comment('orgstaff.id');
            $table->bigInteger('sid')->comment('schedules.id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stf_schedule');
    }
}
