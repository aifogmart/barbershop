<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orgstaff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lname',30);
            $table->string('fname',30);
            $table->string('mname',30)->nullable();
            $table->string('post',30)->nullable();
            $table->string('phone',16)->nullable()->comment('телефон сотрудника');
            $table->boolean('active')->nullable()->default(1);

            $table->timestamp('created_at')->nullable()->useCurrent=true;
            $table->bigInteger('created_by')->nullable()->unsigned()->default(1)
                ->comment('UserID, создавшего запись');
            $table->timestamp('updated_at')->nullable()->useCurrent=true;
            $table->bigInteger('updated_by')->nullable()->unsigned()->default(1)
                ->comment('UserID, изменившего запись');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
        Schema::dropIfExists('orgstaffs');
        Schema::dropIfExists('orgstaff');
    }
}
