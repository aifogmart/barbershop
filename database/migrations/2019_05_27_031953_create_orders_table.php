<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',60)->nullable()
                ->comment('Что-то вроде описания заказ');
            $table->bigInteger('ownorgid')->unsigned()->default(1);
            $table->bigInteger('orgid')->unsigned()->comment('Организация покупатель');
            $table->bigInteger('ownstaffid')->unsigned()->nullable()
                ->comment('Cотрудник пownstaffidродавца, обрабатывающий заказ');
            $table->dateTime("begdt" )->nullable()->comment("Начало заказа");
            $table->dateTime("enddt" )->nullable()->comment("Окончание заказа");


            $table->tinyInteger('statusid')->unsigned()->default(1)
                ->comment('Статус заказа');
            $table->DECIMAL('totalsum',12,2)->default(0)->comment('Cтоимость заказа');

            $table->timestamp('created_at')->nullable()->useCurrent=true;
            $table->bigInteger('created_by')->nullable()->unsigned()->default(1)
                ->comment('UserID, создавшего запись');
            $table->timestamp('updated_at')->nullable()->useCurrent=true;
            $table->bigInteger('updated_by')->nullable()->unsigned()->default(1)
                ->comment('UserID, изменившего запись');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('orders');
    }
}
