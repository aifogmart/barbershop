<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_schedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sid')->comment('id расписания');
            $table->smallInteger('weekdate')->comment('номер дня недели. 0 - вс, 6 - сб');
            $table->smallInteger('beghr')->comment('Час начала работы');
            $table->smallInteger('endhr')->comment('час окончания работы');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('day_schedule');
    }
}
