<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Refitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refitems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',120);
            $table->boolean('active')->default(1);
            $table->text('descript')->nullable();
            $table->decimal('price',12,2)->nullable();
            $table->integer('duration')->nullable()->comment('длительность в минутах');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refitems');
    }
}
