<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orditems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('orditems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ordid')->unsigned();
            $table->bigInteger('refitmid')->unsigned();
            $table->DECIMAL('price',12,2)->default(0)->comment('Цена за единицу товара, руб');
            $table->smallInteger('num')->nullable()->comment("Порядковый номер");

            $table->timestamp('created_at')->nullable()->useCurrent=true;
            $table->bigInteger('created_by')->nullable()->unsigned()->default(1)
                ->comment('UserID, создавшего запись');
            $table->timestamp('updated_at')->nullable()->useCurrent=true;
            $table->bigInteger('updated_by')->nullable()->unsigned()->default(1)
                ->comment('UserID, изменившего запись');

//            $table->foreign('ordid')->references('id')->on('orders');
//            $table->foreign('refitmid')->references('id')->on('refitems');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orditems');
    }
}
