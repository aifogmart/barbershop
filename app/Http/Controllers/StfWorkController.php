<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\schedule;
use App\refitem;

class StfWorkController extends Controller
{
    //
    
    public function getStfWork(){

        $curdate = date( 'd.m.Y', today()->getTimestamp());
		return view("stfwork", compact("curdate"));
	}

	public function getDaySchedule(Request $request){
        return schedule::getSheduleOnDate($request->get("date"));
    }

    public function getStfRefitems(Request $request){
        return refitem::getStfRefitems($request->get("stfid"));
    }

}
