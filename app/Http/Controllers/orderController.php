<?php

namespace App\Http\Controllers;

use App\dlvrytype;
use App\objflag;
use App\objlog;
use App\objfile;
use App\order;
use App\orditem;
use App\ordstaff;
use App\ordstatustype;
use App\Org;
use App\org_curator;
use App\orgstaff;
use App\usrsysright;
use App\wrhdoc;
use Auth;
use DB;
use Illuminate\Http\Request;


class orderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Установка прав пользователя-клиента
     */
    protected function setInterfaceRight($orderid)
    {
        $staffid = Auth::user()->StaffID;
        $userid = Auth::user()->id;

        $usrrights = array();
        $usrrights['save'] = false;
        $usrrights['take'] = false;
        $usrrights['breakwork'] = false;
        $usrrights['delete'] = false;
        $usrrights['complete'] = false;
        $usrrights['back2work'] = false;
        $usrrights['buyerupd'] = false;
        $usrrights['order.copy'] = false;
        $usrrights['order.print'] = false;
        $usrrights['objfiles.read'] = usrsysright::isUserHasRightByCode($userid, 'objfiles.read');
        $usrrights['objfiles.create'] = false;

        //dd($order->id);
        if ($orderid == -1) {
            //Новый заказ - можно сохранять
            $usrrights['save'] = true;
            $usrrights['buyerupd'] = true;
        } else {
            $order = order::select('orgid', 'buyerstaffid', 'ownstaffid', 'statusid')
                ->find($orderid);

            $ownstaffid = $order->ownstaffid;
            if (is_Null($ownstaffid)) {
                //заказ никто не обрабатывает
                $ownstaffid = 0;

                if (objflag::IsSetObjFlag(131, $orderid, 5) == 1) {
                    //заказ подготовлен
                    //Кто может его вернуть в работу? - обладатель права 80 "вернуть в работу завершенный заказ без менеджера"
                    $usrrights['back2work'] = usrsysright::isUserHasRightByCode($userid, 'orders.back2work');
                    $usrrights['order.copy'] = true; //Можно скопировать в новый заказ

                } else {
                    if (usrsysright::isUserHasRight($userid, 61)) {
                        //обладает глобальным правом на кураторство (любого клиента)
                        $usrrights['take'] = true;
                    } // => проверим курирует ли он конкретно этого клиента
                    elseif (org_curator::isStfIsOrgCurator($staffid, $order->orgid)) {
                        $usrrights['take'] = true;
                    }
                }
            } //Кто-то уже обрабатывает этот заказ
            elseif ($ownstaffid == $staffid) {
                //пользователь = текущему агенту по заказу

                if (objflag::IsSetObjFlag(131, $orderid, 5) == 1) {
                    //заказ подготовлен
                    $usrrights['complete'] = false;
                    $usrrights['delete'] = false;
                    $usrrights['save'] = false;
                    $usrrights['breakwork'] = false;
                    $usrrights['complete'] = false;
                    $usrrights['back2work'] = true;
                    $usrrights['order.copy'] = true; //Можно скопировать в новый заказ
                    $usrrights['order.print'] = true;
                    $usrrights['objfiles.create'] = usrsysright::isUserHasRightByCode($userid, 'objfiles.create');


                } else {
                    // заказ в процессе подготовки
                    $usrrights['complete'] = true;
                    //менеджер может удалить заказ, только если заказ не создавн клиентом
                    $usrrights['delete'] = (is_null($order->buyerstaffid));
                    $usrrights['save'] = true;
                    $usrrights['breakwork'] = true;
                    $usrrights['complete'] = true;
                    $usrrights['buyerupd'] = usrsysright::isUserHasRight($userid, 64) == 1;
                }

            } else {
                //обрабатывает этот заказ другой пользователь
                //проверим права на прерывание
                //dd(usrsysright::isUserHasRight($userid, 60));
                if (usrsysright::isUserHasRight($userid, 60)) {
                    $usrrights['breakwork'] = true;
                }
            }
        }


//        dd($usrrights);
        return $usrrights;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = \Auth::user()->id;

        $usrrights = Array(
            'create' => usrsysright::isUserHasRight($userid, 71),
        );

        $sort_params = session('sort_params');
        if (isset($sort_params)) {
            $sort_by = $sort_params['field'];
            $sort_dir = $sort_params['dir'];
        } else {
            $sort_by = 'ord.id';
            $sort_dir = 'asc';
        }

        $orders = order::select('ord.id', 'ord.id as ordnum', 'ord.name', 'ord.rqstsum',
            'ord.totalsum', 'ord.created_at', 'b.name as buyername',
            'st.name as statusname')
            ->selectraw('stafffiobyid(ord.ownstaffid) as ownstaff_fio')
            ->selectraw('ord_flag345info(ord.id) as OrdPrepStatus')
            ->selectraw('ord_stylebyflag(ord.id) as OrdStyle')
            ->from('orders as ord')
            ->join('orgs as b', 'b.id', '=', 'ord.orgid')
            ->leftjoin('ordstatustypes as st', 'st.id', '=', 'ord.statusid')
            ->orderBy($sort_by, $sort_dir)
            ->paginate(5);

        $buyers = Org::select('id', 'name')
            ->where('id', '!=', 1)
            ->orderBy('name')
            ->get();

        $ordbuyers = org::from('orgs as o')
            ->select('name', 'id')
            ->whereRaw('exists( select 1 from orders as ord where ord.orgid=o.id)')
            ->orderBy('name')
            ->get()->pluck("name", "id")->prepend("", "");

        $s_statuscode = "";
        // dd($buyers);

        return view('orders.index', compact('orders', 'buyers', 'ordbuyers', 's_statuscode', 'usrrights'));
    }

    public function index_sort($field, $dir)
    {
        //
        //dd($field, $dir);
        if (!isset($field)) {
            session(['sort_params' => [
                'field' => 'ord.created_at',
                'dir' => 'desc'
            ]]);
        } else {
            session(['sort_params' => [
                'field' => $field,
                'dir' => ($dir == 1) ? 'asc' : 'desc'
            ]]);

        }
        return redirect()->route('orders.search');
    }

    //Поиск
    public function search(Request $request)
    {

        $userid = \Auth::user()->id;

        $usrrights = Array(
            'create' => usrsysright::isUserHasRight($userid, 71),
        );

        $s_ordnum = "";
        $s_ordname = "";
        $s_buyername = "";
        $s_buyerid = "";
        $s_statuscode = "3"; /*3 - не отработанные заказы*/

        if ($request->isMethod('post')) {
            $s_ordnum = $request->get("s_ordnum");
            $s_ordname = $request->get("s_ordname");
            $s_buyername = $request->get("s_buyername");
            $s_buyerid = $request->get("s_buyerid");
            $s_statuscode = $request->get("s_statuscode");

            //сохраним параметры поиска в сессии
            session(['search_setname' => "orders"]);
            session(['search_params' => [
                's_ordnum' => $s_ordnum,
                's_ordname' => $s_ordname,
                's_buyername' => $s_buyername,
                's_buyerid' => $s_buyerid,
                's_statuscode' => $s_statuscode,
            ]]);
        } else {
            if (session('search_setname') == "orders") {
                if (!empty(session('search_params'))) {
                    //восстановим параметры поиска из сессии
                    $search_params = session('search_params');
//                    dd(session('search_setname'));
//                    dd($search_params);
                    $s_ordnum = $search_params['s_ordnum'];
                    $s_ordname = $search_params['s_ordname'];
                    $s_buyername = $search_params['s_buyername'];
                    $s_buyerid = $search_params['s_buyerid'];
                    $s_statuscode = $search_params['s_statuscode'];
                }
            } else
                //зачистим чужие параметры поиска
                session(['search_params' => []]);
        }

        //Если параметры поиска не заданы, то уйдем на index
        if (strlen($s_ordnum . $s_ordname . $s_buyername . $s_buyerid . $s_statuscode) == 0) return redirect()->route('orders.index');

        $sc = "1=1 ";
        if (strlen($s_ordnum) > 0) {
            $sc = $sc . " and ord.id = " . $s_ordnum . "";
        }
        if (strlen($s_ordname) > 0) {
            $sc = $sc . " and ord.name like '%" . mb_strtoupper($s_ordname) . "%'";
        }
        if (strlen($s_buyername) > 0) {
            $sc = $sc . " and o.name like '%" . mb_strtoupper($s_buyername) . "%'";
        }
        if (strlen($s_buyerid) > 0) {
            $sc = $sc . " and ord.orgid =" . $s_buyerid;
        }
        if (strlen($s_statuscode) > 0) {
            if ($s_statuscode == "0") {
                $sc = $sc . " and ord.ownstaffid is null ";
            }
            if ($s_statuscode == "1") {
                $sc = $sc . " and ord.ownstaffid is not null ";
            }
            if ($s_statuscode == "3") {
                $sc = $sc . " and not exists (select 1 from objflags as f where f.sysobjid=131 and f.objid=ord.id and f.flagtypeid=5)";
            }
            if ($s_statuscode == "4") {
                $sc = $sc . " and exists (select 1 from objflags as f where f.sysobjid=131 and f.objid=ord.id and f.flagtypeid=5)";
            }
        }

        $sort_params = session('sort_params');
        if (isset($sort_params)) {
            $sort_by = $sort_params['field'];
            $sort_dir = $sort_params['dir'];
        } else {
            $sort_by = 'ord.id';
            $sort_dir = 'asc';
        }

        $orders = Order::
        select('ord.id', 'ord.id as ordnum', 'ord.name', 'ord.rqstsum',
            'ord.totalsum', 'ord.created_at', 'o.name as buyername',
            'st.name as statusname')
            ->selectraw('stafffiobyid(ord.ownstaffid) as ownstaff_fio')
            ->selectraw('ord_flag345info(ord.id) as OrdPrepStatus')
            ->selectraw('ord_stylebyflag(ord.id) as OrdStyle')
            ->from('orders as ord')
            ->join('orgs as o', 'o.id', '=', 'ord.orgid')
            ->leftjoin('ordstatustypes as st', 'st.id', '=', 'ord.statusid')
            ->whereRaw($sc)
            ->orderBy($sort_by, $sort_dir)
            ->paginate(5);


        $ordbuyers = org::from('orgs as o')
            ->select('name', 'id')
            ->whereRaw('exists( select 1 from orders as ord where ord.orgid=o.id)')
            ->orderBy('name')
            ->get()->pluck("name", "id")->prepend("", "");

        //номер первой записи на странице:
        $rec0 = $orders->currentPage() * $orders->perPage() - $orders->perPage() + 1;
        return view('orders.index', compact('orders', 'rec0', 's_ordnum',
            's_ordname', 's_buyername', 's_buyerid', 's_statuscode', 'ordbuyers', 'usrrights'));
    }

    //Поиск
    public function searchfororg(Request $request, $s_orgid)
    {
        //Отбор заказов только заданного контрагента - временный вариант
//        dd($s_orgid);

        $userid = \Auth::user()->id;

        $usrrights = Array(
            'create' => usrsysright::isUserHasRight($userid, 71),
        );


        $s_buyerid = $s_orgid;
        $s_ordnum = $request->get("s_ordnum");
        $s_ordname = $request->get("s_ordname");
        $s_buyername = $request->get("s_buyername");
//        $s_orgid = $request->get("orgid");
        $s_statuscode = $request->get("s_statuscode");

        //Если параметры поиска не заданы, то уйдем на index
//        if (strlen($s_ordnum . $s_ordname . $s_buyername) == 0) return redirect()->route('orders.index');
        if (strlen($s_orgid . $s_statuscode) == 0) return redirect()->route('orders.index');

        $sc = "";
//        if (strlen($s_ordnum) > 0) {
//            $sc = $sc . " and ord.id = " . $s_ordnum . "";
//        }
//        if (strlen($s_ordname) > 0) {
//            $sc = $sc . " and ord.name like '%" . mb_strtoupper($s_ordname) . "%'";
//        }
//        if (strlen($s_buyername) > 0) {
//            $sc = $sc . " and o.name like '%" . mb_strtoupper($s_buyername) . "%'";
//        }

        if (strlen($s_orgid) > 0) {
            $sc = $sc . " and ord.orgid=" . $s_orgid;
        }

        if (strlen($s_statuscode) > 0) {
            if ($s_statuscode == "0") {
                $sc = $sc . " and ord.ownstaffid is null ";
            }
        }
        if (strlen($sc) > 0) {
            $sc = substr($sc, 5);
        }
        //dd($sc);

        $orders = Order::
        select('ord.id', 'ord.id as ordnum', 'ord.name', 'ord.rqstsum',
            'ord.totalsum', 'o.name as buyername',
            'st.name as statusname')
            ->selectraw('ord_flag345info(ord.id) as OrdPrepStatus')
            ->selectraw('ord_stylebyflag(ord.id) as OrdStyle')
            ->from('orders as ord')
            ->join('orgs as o', 'o.id', '=', 'ord.orgid')
            ->leftjoin('ordstatustypes as st', 'st.id', '=', 'ord.statusid')
            ->whereRaw($sc)
            ->orderBy('ord.id', 'asc')
            ->paginate(5);

        //номер первой записи на странице:
        $rec0 = $orders->currentPage() * $orders->perPage() - $orders->perPage() + 1;
        return view('orders.index', compact('orders', 'rec0', 's_ordnum',
            's_ordname', 's_buyerid', 's_statuscode', 'usrrights'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userid = \Auth::user()->id;
        $staffid = \Auth::user()->StaffID;

        //Значения "по-умолчанию" для новой записи
        $order = new Order();
        $order->id = -1;
        $order->ownorgid = 1;
        $order->ownstaffid = $staffid;
        $order->statusid = 1;
        $order->created_by = $userid;
        $order->updated_by = $userid;

        if (usrsysright::isUserHasRight($userid, 61)) {
            //Глобальный куратор - доступны все клиенты
            $buyers = Org::select('o.id', 'o.name')
                ->from('orgs as o')
                ->where('o.id', '!=', 1)
                ->orderBy('o.name')
                ->get()->pluck("name", "id")->prepend("", "");
        } else {
            //Доступны только курируемые компании
            $buyers = Org::select('o.id', 'o.name')
                ->from('orgs as o')
                ->join('org_curators as c', 'c.orgid', '=', 'o.id')
                ->where('o.id', '!=', 1)
                ->where('c.staffid', $staffid)
                ->where('c.active', 1)
                ->where('c.begdt', '<=', now())
                ->whereRaw('ifnull(c.enddt, now())>=now()')
                ->orderBy('o.name')
                ->get()->pluck("name", "id")->prepend("", "");
        }

        $orditems = [];

        $auxinfo = Order::AuxInfo($order->id);

        //массив с правами на операции в интерфейсе
        $usrrights = $this->setInterfaceRight($order->id);

        return view('orders.edit',
            compact('order', 'buyers', 'orditems', 'auxinfo', 'usrrights'));
    }

    public function copyOrder($ordid)
    {
        //TODO: проверка прав
        $order = order::find($ordid);
        if (isset($order)) {
            $copyres = $order->copyOrder();
        } else {
            $copyres = array();
            $copyres['err'] = 1;
            $copyres['msg'] = "Заказ с id=$ordid не найден";
        }
        $route = "";
        $sd = array();
        if ($copyres['err'] == 0) {
            $route = route('orders.edit', $copyres['ordid']);
            $sd['success'] = "Заказ успешно создан путем копирования";
        } else {
            $route = route('orders.edit', $ordid);
            $sd['error'] = $copyres['msg'];
        }
        return redirect($route)->with($sd);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userid = \Auth::user()->id;

        $order = order::selectraw('o.*')
            ->selectraw('orgnamebyid(o.orgid) as buyername')
            ->selectraw('stafffiobyid(o.buyerstaffid) as buyerstaff_fio')
            ->selectraw('stafffiobyid(o.ownstaffid) as ownstaff_fio')
            ->selectraw('userfiobyid(o.created_by) as created_by_fio')
            ->selectraw('userfiobyid(o.updated_by) as updated_by_fio')
            ->selectraw('ordgrossweight(o.id) as grossweight')
            ->from('orders as o')
            ->where('o.id', $id)->first();

        $order->unShipQty = order::cntUnShipQty($order->id);

        $buyers = Org::select('id', 'name')
            /*->where('id', '!=', 1)*/
            ->orderBy('name')
            ->get()->pluck("name", "id")->prepend("", "");

        $statusname = ordstatustype::getName($order->statusid);
        $ownstaffname = orgstaff::getFIO($order->ownstaffid);
        if (is_null($ownstaffname) or $ownstaffname == '') {
            $ownstaffname = '-беспризорный заказ-';
        }
        $orditems = orditem::getItmsInfo4Order($id);
        $auxinfo = Order::AuxInfo($id);

        //$order->grossweight = 33000;
        $ordgrossweight = $order->grossweight;

        if (!is_null($ordgrossweight)) {
            $reqdlvrytypes = dlvrytype::select('id', 'name', 'minweight', 'optweight', 'maxweight')
                ->selectRaw($ordgrossweight . '-optweight as difopt')
                ->where('active', 1)
                ->whereraw($ordgrossweight . ' BETWEEN minweight AND maxweight')
                /*->orderbyraw('abs(difopt)')*/
                ->orderby('difopt', 'desc')
                ->limit('3')
                ->get();
        } else {
            $reqdlvrytypes = [];
        }

        //$reqdlvrytypes=dlvrytype::get();

        //массив с правами на операции в интерфейсе
        $usrrights = $this->setInterfaceRight($id);

        $ObjFlags = objflag::getFlags4Obj(131, $id);

        $objfiles = objfile::where('sysobjid', 131)
            ->where('objid', $id)->get();

        if (usrsysright::isUserHasRightByCode($userid, 'stock')) {
            $wrhdocs = wrhdoc::where('ordid', $id)
                ->get();
        } else
            $wrhdocs = null;

        return view('orders.edit',
            compact('order', 'buyers', 'statusname', 'auxinfo', 'orditems',
                'reqdlvrytypes', 'usrrights', 'ObjFlags', 'wrhdocs', 'objfiles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userid = \Auth::user()->id;
        $staffid = \Auth::user()->StaffID;
        //
        $request->validate([
            "orgid" => "required",
        ]);
        $mess = "";
        if ($id == -1) {
            $order = new Order([
                "ownstaffid" => $staffid,
                "statusid" => 2,
                "created_at" => now(),
                "created_by" => $userid,
                "updated_at" => now(),
                "updated_by" => $userid,
            ]);
            //dd($order);
            $mess = "Запись о товаре создана";
        } else {
            $order = Order::find($id);
            $mess = "Запись о заказе обновлена";
        }

        $order->name = $request->get('name');
        $order->orgid = $request->get('orgid');
        $order->plndlvryinfo = $request->get('plndlvryinfo');
        $order->orgid = $request->get('orgid');

        $order->updated_by = $userid;
        $order->updated_at = now();
        $order->save();

        objlog::log_info(131, $order->id, 'Сохранение данных заказа', 5);

        //для новой записи сразу добавим пользователя как агента по заказу
        if ($id == -1) {
            ordstaff::AddStaff2Order($staffid, 1, $order->id);
            //возврат в карточку
            return $this::edit($order->id);
        }
        //возврат в список
        return redirect(route('orders.search'))->with('success', $mess);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Order::delete_by_id($id);
        $sd = array();
        if ($res->err == 1) {
            $route = route('orders.edit', $id);
            $sd["error"] = $res->msg;
            objlog::log_info(131, $id, $res->msg, 2);

        } else {
            $route = route('orders.index');
            $sd['success'] = 'Запись о заказе удалена';
        }
        return redirect($route)->with($sd);
    }


    public function createOrderFromJson(Request $request)
    {

        //print "\n";
        //print_r($request->getContent());
        $result = array(
            'code' => '',
            'ordid' => '',
            'errmsg' => "",
        );

        try {
            //Получаем json
//            $newOrderData = json_decode($request->getContent(), true);
            $newOrderData = $request->order;
            //print "\n";
            //print_r($newOrderData);
            $result = order::createWithItems($newOrderData);
        } catch (\Exception $e) {
            $result["code"] = 0;
            if (config('app.debug') == false) {
                $result['errmsg'] = "Ошибка создания заказа";
            } else {
                $result['errmsg'] = $e->getMessage();
            }
        }
        return response()->json($result);
    }

    public function take2work($ordid)
    {
        $rslt = order::TakeOrder2Work($ordid);
        //dd($rslt);
        if ($rslt['success']) {
            return $this::edit($ordid);
        } else {
            return redirect(route('orders.edit', $ordid))->with('warning', $rslt['msg']);
        }
    }

    public function breakwork($ordid)
    {
        $rslt = order::BreakWork($ordid);
        //dd($rslt);
        if ($rslt['success']) {
            return $this::edit($ordid);
        } else {
            return redirect(route('orders.edit', $ordid))->with('warning', $rslt['msg']);
        }
    }

    public function SetComplete($ordid)
    {
        $rslt = order::SetComplete($ordid);

        if ($rslt['success']) {
            return redirect(route('orders.edit', $ordid));
        } else {
            return redirect(route('orders.edit', $ordid))->with('warning', $rslt['msg']);
        }
    }

    public function SetBack2Work($ordid)
    {
        $rslt = order::Back2Work($ordid);

        if ($rslt['success']) {
            return redirect(route('orders.edit', $ordid));
        } else {
            return redirect(route('orders.edit', $ordid))->with('warning', $rslt['msg']);
        }
    }

    public function edtOrdItemsQty($ordid)
    {
        $orditems = orditem::getItmsInfo4Order($ordid);


        return view('orditems.ItemsQty', compact('ordid', 'orditems'));

    }

    public function saveOrdItemsQty(Request $request)
    {

        $orditems = $request->get('orditems');
        foreach ($orditems as $itm) {
            $titm = Orditem::find($itm["id"]);
            $titm->qty = $itm["qty"];
            $titm->totalsum = $titm->price * $titm->qty;
            $titm->save();
        }
        return 1;
    }

    function ord_evntlog($orderid)
    {
        $order = order::find($orderid);
        $buyername = org::select('name')->find($order->orgid)->name;
        $order->buyername = $buyername;
        $ordevents = objlog::from('objlogs as e')
            ->join('users as u', 'u.id', '=', 'e.write_by')
            ->where('sysobjid', 131)
            ->where('objid', $order->id)
            ->select(DB::raw('e.*'), 'u.name as username')
            ->orderby('write_at', 'desc')
            ->paginate(10);
        //номер первой записи на странице:
        $rec0 = $ordevents->currentPage() * $ordevents->perPage() - $ordevents->perPage() + 1;
        return view('orders.ord_evntlog', compact('order', 'ordevents', 'rec0'));
    }

    function OrdersPrint($ordid)
    {
        $order = order::find($ordid);
        $buyername = org::select('name')->find($order->orgid)->name;
        $orditems = orditem::getItmsInfo4Order($ordid);

        return view('orders.print', compact('ordid', 'orditems', 'buyername', 'order'));
    }

    function createWrhDoc3($ordid)
    {
        $res = wrhdoc::createDoc3FromOrder($ordid);
        $id = $res[0];
        if (!is_null($id))
            return redirect(route('wrhdocs.edit', $id));
        else
            return redirect(route('orders.edit', $ordid))->with('error', $res[1]);
    }

    function createFile($ordid)
    {
        return redirect(route('objfiles.create', ['sysobjid'=>131,'objid'=>$ordid]));
    }


}

