<?php

namespace App;

use DateTime;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\orditem;

class order extends Model
{
    use DeleteTrait;
    protected $guarded = [];

    //связь с позициями заказа
    public function items()
    {
        return $this->hasMany(orditem::class,'ordid','id')->with('refitem');
    }

    //связь с покупателем заказа
    public function org()
    {
        return $this->hasOne(Org::class,'id','orgid');
    }

    //связь с представитлем покупателя заказа
    public function buyerstaff()
    {
        return $this->hasOne(orgstaff::class,'id','buyerstaffid');
    }

    public function ownstaff()
    {
        return $this->hasOne(orgstaff::class,'id','ownstaffid');
    }

    //Создание заказа с позициями
    static public function createWithItems($newOrderData)
    {
        $res = Array(
            'code' => 0,
            'ordid' => '',
            'errmsg' => "",
        );
        do {
            $orgid = null;
            if (array_key_exists("orgid", $newOrderData)) {
                $orgid = $newOrderData["orgid"];
            }
            if (!isset($orgid) && !is_numeric($orgid)) {
                $orgid = orgstaff::userOrgID();
            }
            //Формируем заголовок заказа
            $userid = \Auth::user()->id;
            $userOrgID = orgstaff::userOrgID();
            $staffid = \Auth::user()->StaffID;

            $ordinfo = Array(
                "ownorgid" => 1, //Дальойл
                "orgid" => $orgid,
                "name" => "",
                "buyerstaffid" => $staffid,
                "created_by" => $userid,
                "updated_by" => $userid,
                "statusid" => 1
            );
            $order = new static($ordinfo);

            //Формируем список позиций
            $orditems = $newOrderData["orditems"];
            //Проверяем позиции на корректность
            try {


            } catch (Exception $e) {
                break;
            }
            //Начинаем вставлять данные в заказ
            list($res['ordid'], $res['errmsg']) = $order->createOrder($orditems);

            if (!is_null($res['ordid'])) {
                $res['code'] = 1;
                if ($userOrgID == 1) {
                    //TODO: добавить пользователя как менеджера по заказу
                    ordstaff::AddStaff2Order($staffid, 1, $res['ordid']);
                    $res['link'] = "/orders/" . $res['ordid'];
                    //Установим флаг о взятости заказа менеджером
                    //objflag::AddObjFlag(131, $res['ordid'], 3);
                } else {
                    objflag::AddObjFlag(131, $res['ordid'], 1);
//                    $res['link'] = "/client/orders/" . $res['ordid'];
                    $res['link'] = "/home";
                }
            }
        } while (false);
        return $res;
    }


    protected function createOrder($orditems)
    {
        $mess = "";
        try {
            DB::transaction(function () use ($orditems) {
                $this->save();
//                ordeventslog::usr_info($this->id,'Заказ создан из корзины');
                objlog::log_info(131, $this->id,'Заказ создан из Каталога');

                //$list = array();
                foreach ($orditems as $key => $value) {
                    $itm = new orditem();
                    $itm->ordid = $this->id;
                    $itm->refitmid = $value['refitmid'];
                    $itm->rqstqty = $value['qty'];
                    $itm->price = $value['price'];
                    $itm->rqstsum = $itm->rqstqty * $itm->price;
                    $itm->qty = null;
                    $itm->set_base_refitem_info();
                    $itm->buyerstaffid = \Auth::user()->StaffID;
                    //Не оптимально (нужно разобраться как вставить пачкой)
                    orditem::insert($itm->toArray());
                }
                //Здесь долно быть вызывана прцедура обновления
                static::call_Update_ordermetainfo($this->id);
                //Сумм в заказе
            });
        } catch (\Exception $e) {
            $mess = "Ошибка создания заказа";
            Log::error($e->getMessage()."\n".$e->getTraceAsString());
            $this->id = null;
        }
        $res = array($this->id, $mess);
        return $res;
    }



}
