<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class schedule extends Model
{
    public static function getSheduleOnDate($date){
        $date_ = date_create_from_format('d.m.Y H:i', $date.'00:00');
        $dayn = date( 'w', $date_->getTimestamp());

        return DB::table("orgstaff as o")
            ->join("stf_schedule as ss", "ss.stfid", "o.id")
            ->join("day_schedule as ds", "ds.sid", "ss.sid")
            ->where("ds.weekdate", $dayn)
            ->select("o.id", "o.lname", "o.fname", "o.mname", "ds.beghr", "ds.endhr")
            ->get();

    }
}
