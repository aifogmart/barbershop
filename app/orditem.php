<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\order;

class orditem extends Model
{

    protected $guarded = [];



    static public function getItmsInfo4Order($ordid)
    {
        //20190412 SNS. Используем функцию БД refitm_listname
        // для простого формирования название товара
        $rq = static::from('v_orditems as oi')
            ->selectRaw('oi.*')
            ->where("oi.ordid", $ordid)
            ->orderBy('oi.created_at')->orderBy('oi.id')
            ->get();
        return $rq;
    }

    static public function getItmInfo($itmid)
    {
        $rq = static::from('orditems as oi')
            ->join('orders as o', 'o.id', '=', 'oi.ordid')
            ->where('oi.id', $itmid)
            ->select(DB::Raw('oi.*'), 'o.statusid', 'o.orgid');
        return $rq->first();
    }

//связь с прайсом
    public function refitem()
    {
        return $this->belongsTo(RefItem::class,'refitmid','id');
    }


}
