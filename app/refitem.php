<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class refitem extends Model
{
    //
    public static function getStfRefitems($stfid){
        return
            static::from("refitems as r")
            ->join("Stf_refitems as sr", "sr.refitmid", "r.id")
            ->where("sr.stfid", $stfid)
            ->where("r.active", 1)
            ->select("r.id", "r.name", "r.price", "r.duration")->get();
    }
}
